from abc import ABCMeta
from abc import abstractmethod

class ProposalEngine(metaclass=ABCMeta):
    """
    """

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def set_new_state(self, thetas):
        pass