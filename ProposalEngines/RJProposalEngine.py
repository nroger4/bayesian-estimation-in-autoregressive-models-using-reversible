from abc import abstractmethod

import numpy as np

from ProposalEngines.ProposalEngine import ProposalEngine

class RJProposalEngine(ProposalEngine):
    """
    """

    def __init__(
        self
    ):
        """
        """
        
        self.proposal_densities = np.array([1.0, 1.0])

        assert self.within_proposal_engine is not None

    @abstractmethod
    def set_new_state(self, model):
        pass

    def setup_proposal_step(self):
        self.proposal_densities[:] = [1.0, 1.0]

    def set_new_state_within(self, model):
        self.setup_proposal_step()

        model.model_index[1] = model.model_index[0]
        self.within_proposal_engine.set_new_state(model)
