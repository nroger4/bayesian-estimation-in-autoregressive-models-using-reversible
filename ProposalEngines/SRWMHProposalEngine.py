import numpy as np

from ProposalEngines.ProposalEngine import ProposalEngine

class SRWMHProposalEngine(ProposalEngine):
    """
    Symmetric random walk Metropolis-Hastings proposal engine
    theta' ~ N(theta, sigma^2 I)
    """

    def __init__(
        self,
        sigma
    ):
        """
        parameters
        ----------
        n : int
            length of vector theta
        sigma : (n) array_like
            vector of 
        """

        try:
            self.sigma = np.asarray(sigma)
        except Exception as e:
            print("np.asarray(sigma) failed")
            raise e

        self.proposal_densities = np.array([1.0, 1.0])

    def set_new_state(self, model):
        """
        parameters
        ----------
        thetas : (2, n) array_like
            thetas[0] : array of current model parameter vector
            thetas[1] : array in which proposed model parameter vector is to be stored

        theta' = theta + Z
        Z ~ N(0, I*sigma^2)
        """

        assert self.sigma.size == model.thetas[0].size

        # model.thetas[1] = model.thetas[0] + np.random.normal(
        #     0.0,
        #     self.sigma
        # )

        model.thetas[1] = np.random.normal(
            model.thetas[0],
            self.sigma
        )





