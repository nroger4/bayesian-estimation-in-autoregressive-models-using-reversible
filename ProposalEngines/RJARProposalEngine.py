import numpy as np
import scipy as sp
import scipy.stats

from ProposalEngines.RJProposalEngine import RJProposalEngine
from Models.RJARModel import RJARModel

class RJARProposalEngine(RJProposalEngine):
    """
    """

    def __init__(
        self,
        within_proposal_engine,
        draw_random_variable=sp.stats.norm()
    ):
        """
        """

        self.within_proposal_engine = within_proposal_engine
        self.draw_random_variable = draw_random_variable
        super().__init__()

    def set_new_state(self, model):
        """
        Propose moving up with probability 1/2
        else move down
        """

        assert isinstance(model, RJARModel)

        self.setup_proposal_step()

        model_index = model.model_index
        p_max = model.p_max

        change = np.random.choice([-1, 1])
        model_index[1, 0] = model_index[0, 0] + change
        p_curr = model_index[0, 0]
        p_prop = model_index[1, 0]

        # Copy current theta
        model.thetas[1, :] = model.thetas[0, :]

        # If proposed model is out of bounds
        if (p_prop < 0 or p_max < p_prop):
            # rejected in model.check_bounds
            return

        # If going up, draw
        if change > 0:
            draw = self.draw_random_variable.rvs()
            model.thetas[1, p_prop] = draw
            model.thetas[1, p_prop+1:-1] = 0.0
            # P(theta' -> theta) = J(theta' -> theta) = (1/2)
            # P(theta -> theta') = J(theta -> theta') * f_q(v) * (1/det(h)) = (1/2) * f_q(v)
            self.proposal_densities[1] = 1.0
            self.proposal_densities[0] = self.draw_random_variable.pdf(draw)

        # If going down, consider reverse move
        elif change < 0:
            # P(theta' -> theta) = J(theta' -> theta) * f_q(theta_p_curr) * (1/det(h)) = (1/2) * f_q(v)
            # P(theta -> theta') = J(theta -> theta') = (1/2) 
            self.proposal_densities[1] = self.draw_random_variable.pdf(
                model.thetas[0, p_curr]
            )
            self.proposal_densities[0] = 1.0

    def set_new_state_within(self, model):
        super().set_new_state_within(model)

        p = model.model_index[0, 0]

        # Ensure that parameters not in use are zerod out when not moving in dimension
        model.thetas[0, p+1:-1] = 0.0
        model.thetas[1, p+1:-1] = 0.0









        
