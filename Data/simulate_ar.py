import numpy as np
import pandas as pd

np.random.seed(1)

# ar_param = np.array([-0.9])
ar_param = np.array([-0.3, -0.2, 0.5, 0.6, -0.1])
n = 1000
p = ar_param.size
series = np.zeros(n)

# burn-in
for i in range(p, n):
    series[i] = np.dot(ar_param, series[i-p:i][::-1]) + np.random.normal(0.0, 1.0)
series[0:p] = series[n-p:n]
for i in range(p, n):
    series[i] = np.dot(ar_param, series[i-p:i][::-1]) + np.random.normal(0.0, 1.0)

# pd.DataFrame(series).to_csv("./simulated_0.csv", index=None, header=["Value"])
pd.DataFrame(series).to_csv("./simulated_1.csv", index=None, header=["Value"])
