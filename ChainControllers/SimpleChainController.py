import numpy as np

from ChainControllers.ChainController import ChainController
from Models.Model import Model
from ProposalEngines.ProposalEngine import ProposalEngine

class SimpleChainController(ChainController):
    """
    Simple metropolis-hastings based chain controller
    """

    def __init__(
        self,
        model,
        proposal_engine,
        n_iterations=5000,
        verbose=False
    ):
        assert isinstance(model, Model)
        self.model = model

        assert isinstance(proposal_engine, ProposalEngine)
        self.proposal_engine = proposal_engine

        self.current_iteration = 0
        self.n_iterations = n_iterations
        self.verbose = verbose

        self.theta_result = np.zeros(
            (self.n_iterations, self.model.thetas[0].size)
        )
        self.chain_result = np.zeros(
            (self.n_iterations, 3)
        )

    def run_chain(self):
        """
        Perform n iterations
        Burn-in and thinning should be done afterwards
        """

        for self.current_iteration in range(self.n_iterations):
            self.do_iteration()

            if self.verbose:
                self.print_progress()
                
        self.correct_alphas()

    def do_iteration(self):
        """
        Propose a new model according to g(theta -> theta')
        and accept with probability a(theta, theta'),
        record the next state in the result array
        """

        self.proposal_engine.set_new_state(self.model)
        self.model.set_log_prior(1)
        self.model.set_log_likelihood(1)

        self.set_acceptance_probability()

        if np.random.random() < self.alpha:
            self.model.accept_proposal()

        self.record_state()

    def record_state(self):
        """
        Record the current state of the chain in the result array
        """

        self.theta_result[self.current_iteration][:] = self.model.thetas[0]
        self.chain_result[self.current_iteration][0] = self.model.stored_log_likelihoods[0]
        self.chain_result[self.current_iteration][1] = self.model.stored_log_prior_densities[0]
        self.chain_result[self.current_iteration][2] = self.alpha

    def set_acceptance_probability(self):
        """
        Set alpha to
                           pi(theta')   P(theta' -> theta)
        a(theta, theta') =  --------  *  ----------------
                           pi(theta )   P(theta -> theta')

        Where pi is the distribution to sampled from
        """

        result = 1.0

        # L(data | theta') / L(data | theta)
        result *= np.exp(
            self.model.stored_log_likelihoods[1] - self.model.stored_log_likelihoods[0]
        )

        # prior(theta') / prior(theta)
        result *= np.exp(
            self.model.stored_log_prior_densities[1] - self.model.stored_log_prior_densities[0]
        )

        # P(theta' -> theta) / P(theta -> theta')
        result *= self.proposal_engine.proposal_densities[1]
        result /= self.proposal_engine.proposal_densities[0]

        self.alpha = result

    def get_results(self):
        return (self.theta_result, self.chain_result)

