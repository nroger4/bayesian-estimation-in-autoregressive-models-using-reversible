import numpy as np

from ChainControllers.ChainController import ChainController
from Models.Model import Model
from ProposalEngines.ProposalEngine import ProposalEngine

class RJChainController(ChainController):
    """
    Simple reversible jump chain controller
    """

    def __init__(
        self,
        model,
        proposal_engine,
        n_iterations=5000,
        n_within_iterations=20,
        verbose=False
    ):
        assert isinstance(model, Model)
        self.model = model

        assert isinstance(proposal_engine, ProposalEngine)
        self.proposal_engine = proposal_engine

        self.current_iteration = 0
        self.n_iterations = n_iterations
        self.n_within_iterations = n_within_iterations
        self.verbose = verbose

        self.model_index_result = np.zeros(
            (self.n_iterations, self.model.model_index[0].size)
        )
        self.theta_result = np.zeros(
            (self.n_iterations, self.model.thetas[0].size)
        )
        self.chain_result = np.zeros(
            (self.n_iterations, 3)
        )

    def run_chain(self):
        """
        Perform n iterations
        Burn-in and thinning should be done externally
        """

        for self.current_iteration in range(self.n_iterations):
            self.do_iteration()

            if self.verbose:
                self.print_progress()
                
        self.correct_alphas()

    def do_iteration(self):
        """
        Propose a new model according to g(theta -> theta')
        and accept with probability a(theta, theta'),
        record the new state in the result array
        """

        self.proposal_engine.set_new_state(self.model)
        if self.model.check_bounds(1):
            self.model.set_all(1)
            self.set_acceptance_probability()
            self.consider_proposal()
        else:
            self.alpha = 0.0

        for i in range(self.n_within_iterations):
            self.proposal_engine.set_new_state_within(self.model)
            if self.model.check_bounds(1):
                self.model.set_all(1)
                self.set_acceptance_probability()
                self.consider_proposal()
            else:
                self.alpha = 0.0

        self.record_state()

    def record_state(self):
        """
        Record the current state of the chain in the result array
        """

        self.model_index_result[self.current_iteration][:] = self.model.model_index[0]
        self.theta_result[self.current_iteration][:] = self.model.thetas[0]
        self.chain_result[self.current_iteration][:] = [
            self.model.stored_log_likelihoods[0],
            self.model.stored_log_prior_densities[0],
            self.alpha,
        ]

    def set_acceptance_probability(self):
        """
        Set alpha to
                           pi(theta')   J(theta' -> theta)     1    |        |
        a(theta, theta') =  --------  *  ----------------   ------- |det(Jac)|
                           pi(theta )   J(theta -> theta')   f_D(v) |        |

        Where pi is the distribution to be sampled from
        """

        result = 1.0

        # L(data | theta') / L(data | theta)
        result *= np.exp(
            self.model.stored_log_likelihoods[1] - self.model.stored_log_likelihoods[0]
        )

        # prior(theta') / prior(theta)
        result *= np.exp(
            self.model.stored_log_prior_densities[1] - self.model.stored_log_prior_densities[0]
        )

        # All of these are correctly set in the proposal engine
        # P(theta' -> theta) / P(theta -> theta')
        result *= self.proposal_engine.proposal_densities[1]
        result /= self.proposal_engine.proposal_densities[0]

        self.alpha = result

    def get_results(self):
        return (self.model_index_result, self.theta_result, self.chain_result)

        
