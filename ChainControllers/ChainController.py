from abc import ABCMeta
from abc import abstractmethod

import numpy as np

class ChainController(metaclass=ABCMeta):
    """
    Controller of a chain's state
    Controls acceptance and rejection of states, and stores the results
    of the chain's run
    """
    
    @abstractmethod
    def __init__(self):
        # self.model
        # self.proposal_engine
        # self.current_iteration
        # self.n_iterations
        # self.verbose
        # self.theta_result
        # self.chain_result
        pass

    @abstractmethod
    def run_chain(self):
        pass

    @abstractmethod
    def do_iteration(self):
        pass

    @abstractmethod
    def record_state(self):
        pass
        
    @abstractmethod
    def get_results(self):
        pass

    @abstractmethod
    def set_acceptance_probability(self):
        pass

    @abstractmethod
    def get_results(self):
        pass

    def consider_proposal(self):
        if np.random.random() < self.alpha:
            self.model.accept_proposal()

    def correct_alphas(self):
        """
        Set alphas to the probabilities between 0 and 1
        """

        alphas = self.chain_result[:, 2]
        alphas[:] = np.where(alphas > 1.0, 1.0, alphas)

    def print_progress(self):
        """
        Print percent completed iterations every 10 percent
        """

        if self.n_iterations < 10:
            print(self.current_iteration)
        else:
            if (self.current_iteration + 1) % (self.n_iterations//10) == 0:
                print("{0}% done".format(100*(self.current_iteration+1)//self.n_iterations))




