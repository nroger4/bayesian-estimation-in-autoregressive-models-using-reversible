#define EXPORT extern "C" __declspec(dllexport)

static_assert(sizeof(double) == 8, "sizeof(double) == 8");
static_assert(sizeof(double*) == 8, "sizeof(double*) == 8");

#include <cassert> // assert

#include <algorithm> // std::max

EXPORT
void arma_residuals(
    double* y,
    double* residuals,
    int n,
    int max_order,
    int ar_order,
    int ma_order,
    double constant_term,
    double* ar_params,
    double* ma_params
)
{
    assert(max_order >= std::max(ar_order, ma_order));

    #pragma loop(no_vector)
    for (int i = 0; i < max_order; i++)
    {
        residuals[i] = 0.0;
    }

    for (int i = max_order; i < n; i++)
    {
        double predicted = constant_term;

        #pragma loop(no_vector)
        for (int j = 0; j < ar_order; j++)
        {
            predicted += ar_params[j] * y[i-1-j];
        }

        #pragma loop(no_vector)
        for (int j = 0; j < ma_order; j++)
        {
            predicted += ma_params[j] * residuals[i-1-j];
        }

        residuals[i] = y[i] - predicted;
    }
}