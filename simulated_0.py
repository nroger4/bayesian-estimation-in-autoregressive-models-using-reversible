import pickle

import numpy as np
import scipy as sp
import scipy.stats
import statsmodels.api as sm
import pandas as pd

from Models.RJARModel import RJARModel
from ProposalEngines.RJARProposalEngine import RJARProposalEngine
from ProposalEngines.SRWMHProposalEngine import SRWMHProposalEngine
from ChainControllers.RJChainController import RJChainController

np.random.seed(1)

fname = "simulated_0"
path = "./Data/{}.csv".format(fname)
y = pd.read_csv(path)["Value"].values

p_max = 10
model = RJARModel(
    y,
    mu_prior=sp.stats.norm(0.0, 1000.0),
    phi_prior=sp.stats.norm(0.0, 1.0),
    sigma_sqd_prior=sp.stats.invgamma(0.001, scale=0.001),
    p_prior=sp.stats.randint(0, p_max+1),
    p_max=p_max
)

theta_size = model.thetas[0].size

within_proposal_engine = SRWMHProposalEngine(
    theta_size*[0.015]
)
proposal_engine = RJARProposalEngine(
    within_proposal_engine=within_proposal_engine
)

chain_controller = RJChainController(
    model,
    proposal_engine,
    n_iterations=100000,
    n_within_iterations=30,
    verbose=True,
)

chain_controller.run_chain()

model_index_result, theta_result, chain_result = chain_controller.get_results()
result_df = pd.DataFrame(
    np.column_stack([model_index_result, theta_result, chain_result]), 
    columns=(
        ["p"] + 
        ["mu"] + 
        ["theta_{}".format(i) for i in range(1, theta_result.shape[1] - 2 + 1)] + 
        ["sigma"] + 
        ["log-likelhood", "log-prior-density", "alpha"]
    )
)
result_df.to_csv("./Results/{}_result.csv".format(fname), index=None)

