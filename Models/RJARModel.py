import ctypes
import sys

import numpy as np
import scipy as sp
import scipy.linalg
import scipy.stats
import statsmodels.api as sm

from Models.RJModel import RJModel

class RJARModel(RJModel):
    """
    Model to represent autoregressive

    y_t = c + phi*y_t-1:i + epsilon_t
    """

    def __init__(
        self,
        y,
        phi_prior,
        sigma_sqd_prior,
        p_prior,
        mu_prior,
        p_max,
        error_distribution=sp.stats.norm
    ):
        """
        """

        assert isinstance(y, np.ndarray)

        self.y = y

        self.model_index = np.zeros([2, 1], dtype=np.int)
        theta_size = p_max + 2
        self.thetas = np.zeros([2, theta_size])
        self.thetas[0, 0] = y.mean()
        self.thetas[0, -1] = y.std()

        self.p_prior = p_prior
        self.mu_prior = mu_prior
        self.phi_prior = phi_prior
        self.sigma_sqd_prior = sigma_sqd_prior

        self.p_max = p_max

        self.error_distribution = error_distribution

        assert self.check_bounds(0)
        super().__init__()

    def compute_log_prior(self, idx):
        result = 0.0

        p = self.model_index[idx, 0]
        mu = self.thetas[idx, 0]
        phi = self.thetas[idx, 1:p+1]
        sigma = self.thetas[idx, -1]

        result += self.p_prior.logpmf(p)
        result += self.mu_prior.logpdf(mu)
        result += self.phi_prior.logpdf(phi).sum()
        result += self.sigma_sqd_prior.logpdf(sigma**2.0)

        return result

    def compute_log_likelihood(self, idx):
        result = 0.0

        p = self.model_index[idx, 0]
        p_max = self.p_max
        mu = self.thetas[idx, 0]
        phi = self.thetas[idx, 1:p+1]
        sigma = self.thetas[idx, -1]
        y = self.y - mu

        resid = np.zeros(y.size, dtype=np.double)

        assert y.flags.c_contiguous
        assert phi.flags.c_contiguous
        self.lib.arma_residuals(
            y.ctypes.data,
            resid.ctypes.data,
            y.size,
            p_max,
            p,
            0,
            0.0,
            phi.ctypes.data,
            0
        )

        result += sp.stats.norm.logpdf(resid[p_max:], 0.0, sigma).sum()

        return result

    def check_bounds(self, idx):
        """
        Check boundary conditions on parameters
        p_max <- [0, p_max]
        sigma_sqd <- [0, inf)
        
        returns:
            bool : boundary conditions satisfied
        """

        p_max = self.p_max
        p = self.model_index[idx, 0]

        if p < 0 or p_max < p:
            return False

        sigma = self.thetas[idx, -1]

        if sigma <= 0.0:
            return False        

        return True

    try:
        lib = ctypes.cdll.LoadLibrary("./Dll/arma_likelihood.dll")
        lib.arma_residuals.argtypes = [
            ctypes.c_void_p,
            ctypes.c_void_p,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_double,
            ctypes.c_void_p,
            ctypes.c_void_p,
        ]
    except OSError as e:
        print("Could not find arma likelihood dll, falling back to pure python function", file=sys.stderr)
        raise e
