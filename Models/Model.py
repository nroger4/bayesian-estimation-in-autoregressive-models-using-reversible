from abc import ABCMeta
from abc import abstractmethod

import numpy as np

class Model(metaclass=ABCMeta):
    """
    Representation of current and proposed states in a chain.
    Knows how to compute its priors and compute its log likelihood
    """

    @abstractmethod
    def __init__(self):
        """
        theta : (2, theta.size) array_like
        stored_lls (2) array_like
        stored_pds (2) array_like
        """

        self.stored_log_likelihoods = np.zeros(2)
        self.stored_log_prior_densities = np.zeros(2)

        self.set_log_likelihood(0)
        self.set_log_prior(0)

        assert self.thetas is not None

    def accept_proposal(self):
        """
        Accept proposed state as next state
        Adjust stored prior density and stored log likelihood accordingly
        """

        self.thetas[0][:] = self.thetas[1]
        self.stored_log_likelihoods[0] = self.stored_log_likelihoods[1]
        self.stored_log_prior_densities[0] = self.stored_log_prior_densities[1]

    def set_all(self, idx):
        self.set_log_likelihood(idx)
        self.set_log_prior(idx)

    def set_log_likelihood(self, idx):
        self.stored_log_likelihoods[idx] = self.compute_log_likelihood(idx)

    def set_log_prior(self, idx):
        self.stored_log_prior_densities[idx] = self.compute_log_prior(idx)

    @abstractmethod
    def compute_log_likelihood(self, idx):
        pass

    @abstractmethod
    def compute_log_prior(self, idx):
        pass
