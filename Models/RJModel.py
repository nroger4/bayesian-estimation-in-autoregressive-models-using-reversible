import numpy as np

from Models.Model import Model

class RJModel(Model):
    def __init__(self):
        assert self.model_index is not None

        super().__init__()

    def accept_proposal(self):
        self.model_index[0, :] = self.model_index[1, :]
        super().accept_proposal()

